import React from "react";
import './App.css';
import Button from "./component/button/button";
import Modal from "./component/modal/modal";



class App extends React.Component {
    constructor() {
        super();
        this.state = {
            modal: false,
            modal2: false,
        }
        this.content = {
            firstModal: {
                header: "Do you want to delete this file?",
                content: "Once you delete this file, it won’t be possible to undo this action.\n" +
                    "Are you sure you want to delete it?",
                closeButton: true,
            },
            secondModal: {
                header: "Do you want to approve this file?",
                content: "Once you approve this file, it won’t be possible to undo this action.\n" +
                    "Are you sure you want to approve it?",
                closeButton: false,
            }

        }
    }

    showModal = () => {
        this.setState({openModal: true, modal: true,})
    }
    showSecondModal = () => {
        this.setState({openModal: true, modal2: true})
    }
    closeModal = (e) => {
        let cursorTarget = e.target
        console.log(cursorTarget);
        if (cursorTarget.classList.contains("modal-container") || cursorTarget.tagName.toLowerCase() === "button") {
            this.setState({modal2: false, modal: false})
        }
    }
    closeBtn = () => {
        this.setState({openModal: false})
    }

    render() {
        const {modal, modal2} = this.state
        return (
            <div className={"App"}>
                <Button showModal={this.showModal}
                        className ="standardBtn"
                        bgColor={{backgroundColor: "#e74c3c"}}
                        buttonText="open first modal"/>
                <Modal modal={modal}
                       propertis={this.content.firstModal}
                       closeModal={this.closeModal}
                       actions={{
                           moreButton: () => (<div className={'button-container'}>
                               <button className={'standardBtn standardBtn--cancel'} onClick={(e) =>this.closeModal(e)}>Cancel</button>
                               <button className={"standardBtn standardBtn--cancel"} onClick={(e)=>this.closeModal(e)}>OK</button>
                           </div>)
                       }}/>

                <Button showModal={this.showSecondModal}
                        className ="standardBtn"
                        bgColor={{backgroundColor: "#30b44a"}}
                        buttonText="open second window"/>
                <Modal modal2={modal2}
                       closeModal={this.closeModal}
                       propertis={this.content.secondModal}
                       actions={{
                           moreButton: () => (<div className={'button-container'}>
                               <button className={"standardBtn standardBtn--received"} onClick={(e) =>this.closeModal(e)}>Cancel button</button>
                               <button className={"standardBtn standardBtn--received"} onClick={(e)=>this.closeModal(e)}>OK</button>
                           </div>)
                       }}/>
            </div>
        )
    }
}

export default App;
