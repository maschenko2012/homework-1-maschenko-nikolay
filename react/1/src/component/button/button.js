import React, {Component} from 'react';
import buttonStyle from './button.scss'
// import PropTypes from 'prop-types';

class Button extends Component {
    render() {
        const {bgColor, showModal, buttonText, className} = this.props
        return (
            <div>
            <button className={className} style={bgColor} onClick={() =>showModal()}>{buttonText}</button>
            </div>
        );
    }
}

export default Button;