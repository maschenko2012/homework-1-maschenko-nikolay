import React, {Component} from 'react';
import modalStyle from './modal.scss'

class Modal extends Component {
    render() {
        const {modal, modal2, closeModal, propertis,actions} = this.props
        return (
            <>
                {modal &&
                <div className="modal-container" onClick={closeModal}>
                    <div className={'modal__content'}>
                        <h3 className={"modal__title"}>{propertis.closeButton ?
                            <button className={'closeBtn'} onClick={closeModal}>X</button> : null}
                            {propertis.header}
                        </h3>
                        <p className={"modal__description"}>{propertis.content}</p>
                        {actions.moreButton()}
                    </div>
                </div>
                }
                {modal2 &&
                <div className="modal-container" onClick={closeModal}>
                    <div className={'modal__content modal__content--received'}>
                        {propertis.closeButton ?
                            <button className={'closeBtn'} onClick={closeModal}>X</button> : null}
                        <h3 className={'modal__title modal__title--received'}>{propertis.header}</h3>
                        <p className={"modal__description modal__description--received"}>{propertis.content}</p>
                        {actions.moreButton()}
                    </div>
                </div>
                }
            </>

        )
    }
}

export default Modal
