import axios from "axios";
import Loading from "./components/Loading/Loading";
import ListItems from "./components/ListItems/listItems";
import React, {Component} from 'react';
import Card from "./components/Card/card";
import "./App.scss"
import '../src/css/main.scss'
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/button";


class App extends Component {
    constructor() {
        super();
        this.state = {
            products: [],
            isLoading: true,
            number: [],
            isOpenModal: false,
        }
    }

    addFavouriteLocal = (id) =>{
        let array = JSON.parse(localStorage.getItem('favourites')) || []
        array = (array.includes(id) ? array.filter(item =>item!==id):array.concat(id))
        const favArr = JSON.stringify(array)
        localStorage.setItem('favourites', favArr)
    }
    addBuyLocal = (e) =>{
        let id = +e.target.id
        let array = JSON.parse(localStorage.getItem('buy')) || []
        array = array.concat(id)
        const buyArr = JSON.stringify(array)
        localStorage.setItem('buy', buyArr)
        this.setState({...this.state, isOpenModal: false})
    }

    showModal = (e) => {
        this.setState({...this.state, isOpenModal: true})
        let cursorTarget = e.target
        this.setState({number: cursorTarget.id})
    }

    getFavourite  = (id) =>{
        const newArr = this.state.products.map(item =>{
            if (item.vendorСode === id){
                item.isFavourite =!item.isFavourite
            }
            return item
        })
        this.setState({...this.state, products: newArr})
        this.addFavouriteLocal(id)
    }

    closeModal = (e) => {
        if (e.currentTarget.classList.contains('btn--close')||e.currentTarget.classList.contains('modal-container')){
            this.setState( {isOpenModal:false})
        }
    }

    normalizeData = (data) =>{
        data.map(item =>{
            const favourites =  localStorage.getItem('favourites') || []
             item.isFavourite = favourites.includes(item.vendorСode)
             item.added = false
             return item
        })
    }
    getInfo() {
        axios('http://localhost:3000/products.json')
            .then(res => {
                this.normalizeData(res.data)
                this.setState({...this.state, products: res.data})
                this.setState({...this.state, isLoading: false})
            })
    }

    componentDidMount() {
        this.getInfo()
    }

    render() {
        const {isLoading, isOpenModal, products, favourite,} = this.state
        if (isLoading) {
            return (
                <Loading/>
            )
        }
        return (
            <div className={"main"}>
                <ListItems listArr={products}/>
                <Card listArr={products} some = {this.getFavourite} showModal={this.showModal}
                />
                      {isOpenModal?
                          <Modal close = {this.closeModal}
                              actions={{
                          moreButton: () => (<div className="modal-button">
                              <Button cssClass="btn btn--ok" text="Да" id={this.state.number} open={(e) => this.addBuyLocal(e)}/>
                              <Button cssClass="btn btn--cancel" text="Нет" open={() => this.setState({isOpenModal: false})}/>
                          </div>)
                      }}/>:null}
            </div>
        );
    }
}

export default App;


