import React, {Component} from 'react';
import Button from "../Button/button";
import Modal from "../Modal/Modal";
import Star from "../Star/star";
import PropTypes from 'prop-types'
import './card.scss'

class Card extends Component {
    constructor() {
        super();
    }

    render() {
        const {listArr, showModal, some,} = this.props
        return (
            <div className={'gallery-item'}>
                 {listArr.map(el => {
                     return <div className={'gallery-card'} key={el.vendorСode}>
                                <h3 className={'standard-title'}>{el.name}</h3>
                                <div className={'gallery-card__favourite'}><span className={'gallery-card__favourite__text'}>В избранные</span> <Star filled ={el.isFavourite} some = {some} id={el.vendorСode} /></div>
                                <div><img src={`${el.image}`} alt={`${el.name}`}/></div>
                                <p className={'gallery-card__price'}>Cтоимость: <span className={'price-value'}>{el.price}</span></p>
                                <p className={'description'}>{el.color}</p>
                                <div className={'gallery-card__action'}>
                                <Button  cssClass ={'btn btn--card'} text="Добавить в корзину" id ={el.vendorСode} open={(e)=>{showModal(e)}}/></div>
                        </div>
                    })}
            </div>
        );
    }
}

export default Card;

Card.propTypes = {
    listArr:PropTypes.array.isRequired,
    addLocal:PropTypes.func,
}

