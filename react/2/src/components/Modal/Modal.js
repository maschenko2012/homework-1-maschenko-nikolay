import React, {Component} from 'react';
import modalStyle from './modal.scss'
import Button from "../Button/button";
import Proptypes from  'prop-types'
class Modal extends Component {

    render() {
        const {actions, close} = this.props
        return(
            <div className={"modal-container"} onClick={(e)=>close(e)}>
                <div className="modal-content">
                    <h3 className={"standard-title modal__title"}>Информация о товаре</h3>
                    <p className={"modal__description"}>
                        Выбраный вами товар есть в наличии и может быть доставлен в ближайшее к вам отделение новой почты. Нажмити "Да", чтобы перейти к оплате.
                    </p>
                        {actions.moreButton()}
                    <Button text={"X"} cssClass = {'btn btn--close'} open ={close}/>
                </div>
            </div>
        )
    }
}

export default Modal

Modal.prototypes = {
    actions: Proptypes.actions,
    close: Proptypes.func,
}
