import React, {Component} from 'react';
import './button.scss'

class Button extends Component {
    render() {
        const {text, open, id, cssClass} = this.props
        return (
            <button className={cssClass} id ={id} onClick={(e)=>open(e)}>{text}</button>
        );
    }
}

export default Button;