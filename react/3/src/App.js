import axios from "axios";
import {useState, useEffect} from 'react'
import React from 'react';
import "./App.scss"
import '../src/css/main.scss'
import AppRoutes from "./router/AppRoutes";

const App = () => {
    const [products, setProduct] = useState([]);
    const [isLoading, setLoading] = useState(false)
    const [number, setNumber] = useState([])
    const [isOpenModal, setOpenModal] = useState(false)
    const [favourites, setFavourites] = useState([])
    const [buy, setBuy] = useState([])

    const addFavouriteLocal = (id) => {
        let array = JSON.parse(localStorage.getItem('favourites')) || []
        array = (array.includes(id) ? array.filter(item => item !== id) : array.concat(id))
        const favArr = JSON.stringify(array)
        setFavourites(favArr)
        localStorage.setItem('favourites', favArr)
    }

    // const youBuy = (e) =>{
    //     let id = +e.target.id
    //     let array = JSON.parse(localStorage.getItem('buy')) || []
    //     array = (array.includes(id) ? array.filter(item => item !== id) : array.concat(id))
    //     const buyArr = JSON.stringify(array)
    //     localStorage.setItem('buy', buyArr)
    //     setOpenModal(false)
    //     setBuy(buyArr)
    //     return (
    //         <div className={"modal-container"}>
    //             <div className={"modal-content"}>
    //                 <h1>Ты купил</h1>
    //             </div>
    //         </div>
    //     )
    // }
    const getFavourites = () =>{
        let array = JSON.parse(localStorage.getItem('favourites')) || []
        setFavourites(array)
    }
    const addBuyLocal = (e) => {
        let id = +e.target.id
        let array = JSON.parse(localStorage.getItem('buy')) || []
        array = (array.includes(id) ? array.filter(item => item !== id) : array.concat(id))
        const buyArr = JSON.stringify(array)
        localStorage.setItem('buy', buyArr)
        setOpenModal(false)
        setBuy(buyArr)
    }
    const getBuy = () =>{
        let array = JSON.parse(localStorage.getItem('buy')) || []
        setBuy(array)
    }
    const showModal = (e) => {
        setOpenModal(true)
        let cursorTarget = e.target
        setNumber(cursorTarget.id)
    }
    const getFavourite = (id) => {
        const newArr = products.map(item => {
            if (item.vendorСode === id) {
                item.isFavourite = !item.isFavourite
            }
            return item
        })
        setProduct(newArr)
        addFavouriteLocal(id)
    }
    const closeModal = (e) => {
        if (e.currentTarget.classList.contains('btn--close') || e.currentTarget.classList.contains('modal-container') ||e.currentTarget.classList.contains('btn--cancel') ) {
            setOpenModal(false)
        }
    }
    const normalizeData = (data) => {
        data.map(item => {
            const favourites = localStorage.getItem('favourites') || []
            item.isFavourite = favourites.includes(item.vendorСode)
            item.added = false
            return item
        })
    }
    const getInfo = () => {
        axios('/products.json')
            .then(res => {
                normalizeData(res.data)
                setProduct(res.data)
                setLoading(false)
            })
    }
    useEffect(() => {
        getInfo()
        getFavourites()
        getBuy()
    }, [])
    if (isLoading) {
        return <p>LOADING ....</p>
    }
    return (
        <AppRoutes
            showModal={showModal}
            closeModal = {closeModal}
            getFavourite = {getFavourite}
            addBuyLocal ={addBuyLocal}
            addFavouriteLocal = {addFavouriteLocal}
            products = {products}
            isLoading = {isLoading}
            number = {number}
            isOpenModal = {isOpenModal}
            favourites = {favourites}
            getFavourites ={getFavourites}
            buy = {buy}
            // youBuy = {youBuy}
        />
    )
}
export default App





