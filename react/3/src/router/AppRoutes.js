import React from "react";
import Main from "../pages/main/main";
import { Redirect, Route, Switch } from "react-router-dom";
import Favourites from "../pages/Favourites/Favourites";
import Buy from "../pages/buy/buy";


const AppRoutes = (props) => {
    console.log(props);
    const {getFavourites,
        number,
        products,
        showModal,
        closeModal,
        getFavourite,
        addBuyLocal,
        addFavouriteLocal,
        favourites,
        removeFavourite,
        isOpenModal,
        buy,
        youBuy,
    } = props

    const authenticated = !!props.user
    console.log('authenticated', authenticated)

    return (
        <div className="app-routes">
            <Switch>
                <Redirect exact from={'/'} to={'/main'}/>
                <Route path={'/main'} exact
                       render={(routerProps) => (
                           <Main
                               isOpenModal ={isOpenModal}
                               showModal ={showModal}
                               closeModal = {closeModal}
                               getFavourite = {getFavourite}
                               addBuyLocal = {addBuyLocal}
                               addFavouriteLocal = {addFavouriteLocal}
                               products = {products}
                               number = {number}
                           />
                      )}
                />
                <Route path={'/favourites'} exact render={() =>(
                     <Favourites
                         isOpenModal ={isOpenModal}
                         showModal ={showModal}
                         favourites = {favourites}
                         getFavourites = {getFavourites}
                         listArr ={products}
                         removeFavourite ={removeFavourite}
                         some ={getFavourite}
                         number = {number}
                         closeModal ={closeModal}
                         addBuyLocal = {addBuyLocal}
                    />
                    )}/>
                    <Route path={'/buy'} exact render={() =>(
                        <Buy
                            addBuyLocal = {addBuyLocal}
                            closeModal = {closeModal}
                            isOpenModal ={isOpenModal}
                            showModal ={showModal}
                            favourites = {favourites}
                            getFavourites = {getFavourites}
                            listArr ={products}
                            removeFavourite ={removeFavourite}
                            some ={getFavourite}
                            number = {number}
                            buy = {buy}
                            youBuy ={youBuy}
                        />
                    )}/>
            </Switch>
        </div>
    );
};

const ProtectedRoutes = ({authenticated, ...rest}) => {
    if (authenticated) {
        return <Route {...rest} />
    }
    return <Redirect to='/login'/>
}

export default AppRoutes;