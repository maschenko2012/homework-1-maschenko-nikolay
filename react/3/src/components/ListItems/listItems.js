import React, {Component} from 'react';
import './listitems.scss'
import PropTypes from 'prop-types'

class ListItems extends Component {
    render() {
        const {listArr} = this.props
        return (
            <ul className={'productsList'}>
                {listArr.map(item => {
                    return <li className={"nav-item"} key={item.vendorСode}><a className={'nav-item__href'} href="#">{item.name}</a></li>
                })}
            </ul>
        )
    }
}

export default ListItems;

ListItems.prototypes = {
    listArr:PropTypes.array.isRequired,
}