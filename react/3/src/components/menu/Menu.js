import React from 'react';
import {NavLink} from 'react-router-dom';
import navCss from './menu.scss'

const Menu = () => {
    return (
        <header className={'header'}>
            <div className="header__wrapper">
                <a href={'#'} className="header__logo">
                    <img src={'/img/logo.png'} className={'logo-img'}/>
                </a>
                <ul className="ul-row">
                     <li className="ul-row__item ul-row__item--nav"><NavLink className="ul-row__item__href" activeClassName="ul-row__item__href--active" to='/main'>Главная</NavLink></li>
                     <li className="ul-row__item ul-row__item--nav"><NavLink className="ul-row__item__href" activeClassName="ul-row__item__href--active" to='/favourites'>Избранные</NavLink></li>
                     <li className="ul-row__item ul-row__item--nav"><NavLink className="ul-row__item__href" activeClassName="ul-row__item__href--active" to='/buy'>Корзина</NavLink></li>
                 </ul>
            </div>
        </header>
    );
}

export default Menu;