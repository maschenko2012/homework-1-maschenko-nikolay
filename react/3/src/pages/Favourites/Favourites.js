import React from 'react';
import Card from "../../components/Card/card";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/button";
import Menu from "../../components/menu/Menu";
import favouritesStyle from './favourites.scss'


const Favourites = (props) => {
    const {showModal, favourites, listArr, some, isOpenModal, closeModal, number, addBuyLocal} = props
    const newCard = listArr.filter(item => favourites.includes(item.vendorСode));
    return (
        <>
            <header>
                <Menu/>
            </header>

                <div className="gallery-item">
                    <Card listArr={newCard} newCard={newCard} some={some} showModal={showModal}
                    />
                </div>
                {isOpenModal ?
                    <Modal close={closeModal}
                           actions={{
                               moreButton: () => (<div className="modal-button">
                                   <Button cssClass="btn btn--ok" text="Да" id={number}
                                           open={(e) => addBuyLocal(e)}/>
                                   <Button cssClass="btn btn--cancel" text="Нет" open={(e) => closeModal(e)}/>
                               </div>)
                           }}/> : null}

        </>
    );
}

export default Favourites;