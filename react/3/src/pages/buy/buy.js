import React from 'react';
import Card from "../../components/Card/card";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/button";
import Menu from "../../components/menu/Menu";
import buyCss from './buy.scss'

const Buy = (props) => {
    const {showModal, listArr, some, closeModal, number, addBuyLocal, isOpenModal, buy} = props

    const newCard = listArr.filter(item => buy.includes(item.vendorСode));
    return (
        <>
            <header>
                <Menu/>
            </header>
            <div className="gallery-item gallery-item--container">
                <Card listArr={newCard} newCard={newCard} some={some} showModal={showModal}
                      closeBtn={true} addBuyLocal={addBuyLocal}
                />
            </div>
            {isOpenModal ?
                <Modal close={closeModal}
                       actions={{
                           moreButton: () => (
                               <div className="modal-button">
                                   <Button cssClass="btn btn--ok" text="Да" id={number}
                                           open={(e) => addBuyLocal(e)}/>
                                   <Button cssClass="btn btn--cancel" text="Нет"
                                           open={(e) => closeModal(e)}/>
                               </div>)
                       }}/> : null}
        </>
    )
        ;
};
// <Button cssClass="btn btn--ok" text="Да" id={item.vendorСode} key={item.vendorСode}
//         open={(e) => addBuyLocal(e)}/>
export default Buy;