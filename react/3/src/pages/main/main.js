import React from "react";
import ListItems from "../../components/ListItems/listItems";
import Card from "../../components/Card/card";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/button";
import Menu from "../../components/menu/Menu";

const Main = (props) => {
    const {products, getFavourite, showModal, isOpenModal, closeModal, number, addBuyLocal, youBuy} = props
    console.log(isOpenModal);
    return (
        <>
            <Menu/>
            <div className={"main"}>
                <ListItems listArr={products}/>
                <div className={'gallery-item'}>
                    <Card listArr={products} some={getFavourite} showModal={showModal}/>
                </div>
                {isOpenModal ?
                    <Modal close={closeModal}
                           actions={{
                               moreButton: () => (<div className="modal-button">
                                   <Button cssClass="btn btn--ok" text="Да" id={number} open={(e) => addBuyLocal(e)}/>
                                   <Button cssClass="btn btn--cancel" text="Нет" open={(e) => closeModal(e)}/>
                               </div>)
                           }}/> : null}
            </div>
        </>
    )
        ;

}

export default Main;