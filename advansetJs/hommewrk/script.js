// Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
// На странице должен находиться div с id="root", куда и нужно будет положить этот список (похожая задача была дана в модуле basic).
// Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться все три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте.
//     Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];
const container = document.querySelector('#root')
console.log(container);


books.forEach(item => {
    try {
        if (item.name === undefined) {
           throw new Error('add name ')
        }
        if (item.price === undefined) {
            throw new Error('add price')
        }
        if (item.author === undefined) {
            throw new Error('add author')
        }
        else {
            container.insertAdjacentHTML('afterbegin', `<ul><li>${item.author}</li><li>${item.name}</li><li>${item.price}</li></ul>`)
        }
    }catch(e) {
        console.log(e);

    }
})

