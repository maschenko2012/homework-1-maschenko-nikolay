// Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.
//     Создайте геттеры и сеттеры для этих свойств.
//     Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
// Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженноен а 3.
// Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.

class Employee{
    constructor(elements) {
        this.name = elements.name;
        this.age = elements.age;
        this.salary = elements.salary;
    }
    get name(){ return this._name}
    set name(value){ return this._name = value};
    get age(){ return this._age}
    set age(value){ return this._age = value};
    get salary(){ return this._salary}
    set salary(value){ return this._salary = value};
}
class Programmer extends Employee{
    constructor(elements) {
        super(elements);
        this.lang = elements.lang
    }
    get salary(){ return this._salary*3}
    set salary(value){ return this._salary = value};
}

let some1 = new Programmer({
    name:'jons',
    age: 13,
    salary: 200,
    lang:'Russian'
})
let some2 = new Programmer({
    name:'jons',
    age: 13,
    salary: 200,
    lang:'Russian'
})
let some3 = new Programmer({
    name:'jons',
    age: 13,
    salary: 200,
    lang:'Russian'
})
console.log(some1, some2,some3)
