// Реализовать функцию подсветки нажимаемых клавиш. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     В файле index.html лежит разметка для кнопок.
//     Каждая кнопка содержит в себе название клавиши на клавиатуре
// По нажатию указанных клавиш - та кнопка, на которой написана эта буква, должна окрашиваться в синий цвет. При этом, если какая-то другая буква уже ранее была окрашена в синий цвет - она становится черной. Например по нажатию Enter первая кнопка окрашивается в синий цвет. Далее, пользователь нажимает S, и кнопка S окрашивается в синий цвет, а кнопка Enter опять становится черной.



const btnItems = document.querySelectorAll(".btn");

document.addEventListener('keydown', event =>{

    let eventKeys = event.key;
    btnItems.forEach(item =>{
        if(item.classList.contains('btn-decoration')){
            item.classList.remove('btn-decoration')
        }
        if (eventKeys.toUpperCase() === item.textContent){
            item.classList.add('btn-decoration');
        }
        if (eventKeys === btnItems[0].textContent){
            btnItems[0].classList.add('btn-decoration');
        }
    })
})
