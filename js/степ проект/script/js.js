const whichElementGetContainer = document.querySelector('.our-services-category');
const whichElementGetClass = document.querySelectorAll('.our-services-category-selection');
whichElementGetContainer.addEventListener('click', (e) => {
    const cursorTarget = e.target;
    whichElementGetClass.forEach(item => {
        if (item.classList.contains('our-services-category-selection-active')) {
            item.classList.remove('our-services-category-selection-active');
        }
    });
    cursorTarget.classList.add('our-services-category-selection-active');

    const categoryDescription = document.querySelectorAll('.our-services-description-container');
    const showCategoryDescription = (numberItem) =>{
        categoryDescription.forEach(item => {
            if (item.classList.contains('our-services-description-container-category-show')) {
                item.classList.remove('our-services-description-container-category-show');
            }
        })
        categoryDescription[numberItem].classList.add('our-services-description-container-category-show');
    }
    if (cursorTarget === document.querySelector("#web-item")) {
        showCategoryDescription(0)
    }
    else if(cursorTarget === document.querySelector("#graphic-design-item")) {
        showCategoryDescription(1)
    }
    else if(cursorTarget === document.querySelector("#online-support-item")) {
        showCategoryDescription(2)
    }
    else if(cursorTarget === document.querySelector("#app-design")) {
        showCategoryDescription(3)
    }
    else if(cursorTarget === document.querySelector("#online-marketing")) {
        showCategoryDescription(4)
    }
    else if (cursorTarget === document.querySelector("#seo-service")) {
        showCategoryDescription(5)
    }
})

// amazing-gallery


const categorySwichContainer = document.querySelector('.amazing-category');
const categorySwich = document.querySelectorAll('.amazing-category-item');
categorySwichContainer.addEventListener('click', (event) => {
    const cursorTarget = event.target;
    if (event.target.tagName.toLowerCase() === "button") {
        categorySwich.forEach(item => {
            if (item.classList.contains('amazing-category-item-active')) {
                item.classList.remove('amazing-category-item-active')
            }
        })
        cursorTarget.classList.add('amazing-category-item-active')
    }
});

const amazingCategory = document.querySelector('.amazing-work-container')

amazingCategory.addEventListener('click', (event) => {
    if (event.target === document.getElementById('all-item')) {
        const firstShow = amazingCategoryContainerArr.slice(0, 12);
        amazingCategoryContainerArr.forEach(item => {
            item.classList.remove('amazing-gallery-item-show');
        })
        firstShow.forEach(item => {
            item.classList.add('amazing-gallery-item-show');
        })
        document.getElementById('load-more').classList.add('load-visibility');
    } else if (event.target === document.getElementById('graphic-design')) {
        showAmazingCategory('graphic-design-item');
        document.getElementById('load-more').classList.remove('load-visibility');
    } else if (event.target === document.getElementById('web-design')) {
        showAmazingCategory('web-design-item');
        document.getElementById('load-more').classList.remove('load-visibility');
    } else if (event.target === document.getElementById('landing-pages')) {
        showAmazingCategory('landing-pages-item')
        document.getElementById('load-more').classList.remove('load-visibility');

    } else if (event.target === document.getElementById('wordpress')) {
        showAmazingCategory('wordpress-item')
        document.getElementById('load-more').classList.remove('load-visibility');
    } else if (event.target === document.getElementById('load-more')) {
        const secondShow = amazingCategoryContainerArr.slice(0, 24);
        secondShow.forEach(item => {
            item.classList.add('amazing-gallery-item-show')
        })
        document.getElementById('load-more').classList.remove('load-visibility')
    }
});

const amazingGalleryItem = document.querySelectorAll('.amazing-gallery-item');
const amazingCategoryContainerArr = [...amazingGalleryItem];


const showAmazingCategory = (showClass) => {
    amazingCategoryContainerArr.forEach(item => {
        item.classList.remove('amazing-gallery-item-show')
    });
    const showItem = amazingCategoryContainerArr.filter(element => element.classList.contains(showClass));
    showItem.forEach(item => {
        item.classList.add('amazing-gallery-item-show');
    })
}



let slideIndex = 3;
showSlides(slideIndex);

function plusSlide() {
    showSlides(slideIndex += 1);
}

function minusSlide() {
    showSlides(slideIndex -= 1);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    let slides = document.getElementsByClassName("about-us-item");
    let dots = document.getElementsByClassName("about-us-dot");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (let i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (let i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" slider-dot-active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " slider-dot-active";
}











