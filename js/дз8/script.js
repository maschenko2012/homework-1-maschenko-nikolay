// При загрузке страницы показать пользователю поле ввода (input) с надписью Price.
//     Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При
// потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span, в
// котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним
// должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение,
//     введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода
// красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//
//
//     В папке img лежат примеры реализации поля ввода и создающегося span.

const textPrise = document.createElement('span');
const errorPrice = document.createElement('span');
errorPrice.classList.add('error-price-decoration');
errorPrice.textContent = "Please enter correct price";

const container = document.createElement('div');
container.classList.add('container');

const labelShow = document.createElement('label');
labelShow.textContent = 'prise';
labelShow.classList.add('label-decoration');

const inputShow = document.createElement('input');
inputShow.classList.add('input-decoration');
inputShow.type ="number";
inputShow.placeholder = "enter price"

const buttonShow = document.createElement('button')
buttonShow.textContent = 'x';
buttonShow.classList.add('button-decoration');


inputShow.addEventListener('focus', e => {
    if (e.target === inputShow) {
        inputShow.classList.add('input-decoration-focus')
    }
})
inputShow.addEventListener('blur', e => {
    if (e.target !== buttonShow) {
        if (inputShow.value <= 0) {
            inputShow.classList.add('input-error');
            textPrise.remove();
            container.append(errorPrice);
            inputShow.classList.remove('input-true');

        } else if (inputShow.value >= 0) {
            inputShow.classList.remove('input-decoration-focus');
            textPrise.textContent = `price ${inputShow.value} $`;
            inputShow.classList.remove('input-error');
            inputShow.classList.add('input-true');
            textPrise.classList.add('price-decoration');
            container.append(textPrise);
            errorPrice.remove()
        }
    }
})

buttonShow.addEventListener('click', e => {
    inputShow.value = '';
    inputShow.classList.remove('input-error');
    inputShow.classList.remove('input-true');
    inputShow.classList.remove('input-decoration-focus');
    textPrise.remove();
    errorPrice.remove();
})

document.body.prepend(container)
container.prepend(labelShow, inputShow, buttonShow)