// Технические требования:
//
//     В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
//     Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
//     Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

let test = document.querySelector('.active');


const container = document.querySelector(".tabs");
const containerCategory = document.querySelectorAll(".tabs-title");
const containerItems = document.querySelectorAll('.tabs-item');


container.addEventListener('click', event => {
    const cursorTarget = event.target
    containerCategory.forEach(item => {
        if (item.classList.contains('active')) {
            item.classList.remove('active')
        }
    })
    cursorTarget.classList.add('active');
    containerItems.forEach(item =>{
        if (cursorTarget.getAttribute ('data-name') === item.getAttribute('data-name')){
            item.classList.add('tabs-item-active')
    }
        if (cursorTarget.getAttribute ('data-name') !== item.getAttribute('data-name')){
            item.classList.remove('tabs-item-active')
        }
    })

})

