// Написать реализацию кнопки "Показать пароль". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     В файле index.html лежит разметка для двух полей ввода пароля.
//     По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
//     Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения
//
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.

const inputArr = document.querySelectorAll('.password-decoration');

const showInputValue = document.querySelectorAll('.icon-password')

const form = document.querySelector(".password-form");
const button = document.querySelector('.btn');
const tryAgain = document.createElement("span");

form.addEventListener('click', event => {
    const cursorTarget = event.target;
    if (cursorTarget === button) {
        if (inputArr[0].value === inputArr[1].value && inputArr[0].value.length !== 0) {
            tryAgain.remove()
            alert('You are welcome')
        } else if (cursorTarget === button && form.childNodes.length < 8) {
            tryAgain.style.color = "red"
            tryAgain.textContent = "You must enter the same values";
            tryAgain.classList.add('span-decoration');
            button.before(tryAgain);
        }
    } else if (cursorTarget === showInputValue[0] || cursorTarget === showInputValue[1]) {
        inputArr.forEach(item => {
            if (item.type === 'password') {
                item.type = "text"
            } else {
                item.type = 'password'
            }
        })
    }
});

const test = document.querySelector('.password-form')
console.log(test.childNodes);
