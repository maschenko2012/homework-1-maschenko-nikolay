const menuBtn = document.querySelector('.nav-btn')
const menuContent = document.querySelector('.nav_ul')


menuBtn.addEventListener('click',  (event) => {
    menuContent.classList.toggle('nav_ul__opened')
    menuBtn.classList.toggle('nav-btn__active')
})